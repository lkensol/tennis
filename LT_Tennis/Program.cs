﻿using System;
using System.Text;
using System.Threading;

namespace LT_Tennis
{
    class Program
    {
        //Initilize variables

        static char ball = 'O';
        static int ballPositionX = 0;
        static int ballPositionY = 0;
        static bool ballDirectionUp = true;
        static bool ballDirectionRight = false;

        static char playerBody = '_';
        static int playerWidth = 6;
        static int playerPosition = 0;
        // Random randomGenerator = new Random();

        static bool gameRunning = true;

        static void Main(string[] args)
        {

            Console.CursorVisible = false;
            RemoveScrollBar();
            SetInitionalPositions();

            // Game Ranning

            while (gameRunning)
            {
                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo keyInfo = Console.ReadKey();
                    if (keyInfo.Key == ConsoleKey.LeftArrow)
                    {
                        MovePlayerLeft();
                    }
                    if (keyInfo.Key == ConsoleKey.RightArrow)
                    {
                        MovePlayerRight();
                    }
                    if (keyInfo.Key == ConsoleKey.Escape)
                    {
                        gameRunning = false;
                    }
                }
                MoveBall();
                Console.Clear();
                DrawPlayer();
                DrawBall();
                Thread.Sleep(100);

            }
        }

        // Remove Scrollbar

        static void RemoveScrollBar()
        {
            Console.BufferHeight = Console.WindowHeight;
            Console.BufferWidth = Console.WindowWidth;
        }

        //Print Player Position
        static void PrintAtPosition(int x, int y, char symbol)
        {
            Console.SetCursorPosition(x, y);
            Console.WriteLine(symbol);
        }

        // Draw Player
        static void DrawPlayer()
        {
            for (int i = 1; i < playerWidth; i++)
            {
                PrintAtPosition(playerPosition+i, Console.WindowHeight-2, playerBody);
            }
        }

        // Set initional position player
        static void SetInitionalPositions()
        {
            playerPosition = Console.WindowWidth / 2;
            SetBallPositions();
        }

        // Set initional positiion ball
        static void SetBallPositions()
        {
            ballPositionX = Console.WindowWidth / 2;
            ballPositionY = Console.WindowHeight / 2;
        }

        // Draw Ball
        static void DrawBall()
        {
            PrintAtPosition(ballPositionX, ballPositionY, ball);
        }

        // Move Player

        static void MovePlayerLeft()
        {
            if (playerPosition > 3)
            {
                playerPosition -= 3;
            }
        }

        static void MovePlayerRight()
        {
            if (playerPosition < Console.WindowWidth - (playerWidth + 3))
            {
                playerPosition += 3;
            }
        }

        // Move Ball

        static void MoveBall()
        {
            if (ballPositionY == 1)
            {
                ballDirectionUp = false;
            }

            if (ballPositionY == Console.WindowHeight - 2)
            {
                ballDirectionUp = true;
            }

            if (ballPositionY == Console.WindowHeight - 2 && (ballPositionX < playerPosition || ballPositionX > (playerPosition + playerWidth)))
            {
                Console.Clear();
                Console.SetCursorPosition((Console.WindowWidth / 2) / 2, Console.WindowHeight / 2);
                Console.WriteLine("****Game over ****");
                Console.ReadKey();
            }


            if (ballPositionX == 1)
            {
                ballDirectionRight = true;
                ballDirectionUp = true;
            }
            if (ballPositionX == Console.WindowWidth - 2)
            {
                ballDirectionRight = false;
                ballDirectionUp = false;
            }


            if (ballDirectionUp)
            {
                ballPositionY--;
            }
            else
            {
                ballPositionY++;
            }

            if (ballDirectionRight)
            {
                ballPositionX++;
            }
            else
            {
                ballPositionX--;
            }
        }
    }
}
